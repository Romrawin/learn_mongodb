const mongoose = require('mongoose')
// const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
// Update
//   const room = await Room.findById('6229b52a27c43a1d488437c8')
//   room.capacity = 20
//   room.save()
//   console.log(room)
  const room = await Room.findOne({ capacity: { $gte: 100 } })
  console.log(room)
  console.log('-----------------')
  const rooms = await Room.find({ capacity: { $gte: 100 } })
  console.log(rooms)
}

main().then(() => {
  console.log('Finish')
})
