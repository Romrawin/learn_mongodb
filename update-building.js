const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  const newInformaticsBuilding = await Building.findById('6229b52a27c43a1d488437c7')
  const room = await Room.findById('621b958020631fad5b9f31b1')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
}

main().then(() => {
  console.log('Finish')
})
